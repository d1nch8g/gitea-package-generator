pwd := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

# Generate arch packages in clean arch docker
arch:
	docker build -t arch-packager .

	docker run --rm -it -v ${pwd}/b-any-v1:/home/makepkg arch-packager bash -c "sudo chmod a+rwx /home/makepkg && makepkg"
	docker run --rm -it -v ${pwd}/a-x86_64-v1:/home/makepkg arch-packager bash -c "sudo chmod a+rwx /home/makepkg && makepkg"
	docker run --rm -it -v ${pwd}/a-i686-v1:/home/makepkg arch-packager bash -c "sudo chmod a+rwx /home/makepkg && sudo sed -i 's|x86_64|i686|g' /etc/makepkg.conf && makepkg"
	docker run --rm -it -v ${pwd}/a-i686-v2:/home/makepkg arch-packager bash -c "sudo chmod a+rwx /home/makepkg && sudo sed -i 's|x86_64|i686|g' /etc/makepkg.conf && makepkg"
