FROM docker.io/archlinux/archlinux:base-devel

RUN pacman -Syu --needed --noconfirm git pacman-contrib

ARG user=makepkg
RUN useradd --system --create-home $user
RUN echo "$user ALL=(ALL:ALL) NOPASSWD:ALL" > /etc/sudoers.d/$user
USER $user
WORKDIR /home/$user
